[Download on the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/com.carolynvs.deployment-notifications)

As of v5.5, Bamboo only provides two notifications: on start and on completed. This is a plugin for Bamboo which
provides additional deployment notifications which work exactly the same way as their build notification counterparts.

* Failed Deployments And First Successful
* After X Deployment Failures